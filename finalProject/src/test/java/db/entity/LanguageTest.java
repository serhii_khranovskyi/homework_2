package db.entity;

import com.epam.rd.khranovskyi.dao.entity.Language;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class LanguageTest {

    @Test
    void getCode(){
        assertEquals(2, Language.ENGLISH.getCode());
    }

    @Test
    void get(){
        assertEquals(Language.ENGLISH, Language.get("ENGLISH"));
    }

    @Test
    void getAdmin(){
        assertEquals(Language.RUSSIAN, Language.get("RUSSIAN"));
    }
}
