package db.entity;

import com.epam.rd.khranovskyi.dao.entity.Date;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class DateTest {

    @Test
    void getStart(){
        Date date= new Date();
        date.setStart("2014");
        assertEquals("2014", date.getStart());
    }

    @Test
    void setStart(){
        Date date= new Date();
        date.setStart("2014");
        assertEquals("2014", date.getStart());
    }

    @Test
    void setEnd(){
        Date date= new Date();
        date.setStart("2014");
        assertEquals("2014", date.getStart());
    }

    @Test
    void getEnd(){
        Date date= new Date();
        date.setEnd("2014");
        assertEquals("2014", date.getEnd());
    }

    @Test
    void dateToString(){
        Date date = new Date("2014","2015");
        assertEquals("Date[start = 2014; end = 2015 ]", date.toString());
    }
}
