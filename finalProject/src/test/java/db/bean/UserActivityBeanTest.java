package db.bean;
import com.epam.rd.khranovskyi.dao.bean.UserActivityBean;
import org.junit.jupiter.api.Test;

import java.sql.Timestamp;

import static org.junit.jupiter.api.Assertions.assertEquals;
public class UserActivityBeanTest {
    @Test
    void getBegin() {
        UserActivityBean bean = new UserActivityBean();
        bean.setBegin(Timestamp.valueOf("1000-01-01 00:01:00.0"));
        assertEquals(Timestamp.valueOf("1000-01-01 00:01:00.0"),bean.getBegin());
    }

    @Test
    void getName() {
        UserActivityBean bean = new UserActivityBean();
        bean.setName("test");
        assertEquals("test",bean.getName());
    }

    @Test
    void setBegin() {
        UserActivityBean bean = new UserActivityBean();
        bean.setBegin(Timestamp.valueOf("1000-01-01 00:01:00.0"));
        assertEquals(Timestamp.valueOf("1000-01-01 00:01:00.0"),bean.getBegin());
    }

    @Test
    void setName() {
        UserActivityBean bean = new UserActivityBean();
        bean.setName("test");
        assertEquals("test",bean.getName());
    }

    @Test
    void getEnd() {
        UserActivityBean bean = new UserActivityBean();
        bean.setEnd(Timestamp.valueOf("1000-01-01 00:01:00.0"));
        assertEquals(Timestamp.valueOf("1000-01-01 00:01:00.0"),bean.getEnd());
    }

    @Test
    void setEnd() {
        UserActivityBean bean = new UserActivityBean();
        bean.setEnd(Timestamp.valueOf("1000-01-01 00:01:00.0"));
        assertEquals(Timestamp.valueOf("1000-01-01 00:01:00.0"),bean.getEnd());
    }

    @Test
    void setAllTime() {
        UserActivityBean bean = new UserActivityBean();
        bean.setAllTime(10_000L);
        assertEquals(10_000L,bean.getAllTime());
    }

    @Test
    void getAllTime() {
        UserActivityBean bean = new UserActivityBean();
        bean.setAllTime(10_000L);
        assertEquals(10_000L,bean.getAllTime());   }
    @Test
    void beanToString() {
        UserActivityBean bean = new UserActivityBean();
        bean.setName("test");
        bean.setBegin(Timestamp.valueOf("1000-01-01 00:01:00.0"));
        bean.setEnd(Timestamp.valueOf("1000-01-01 00:01:00.0"));
        assertEquals("UserActivityBean{" +
                "name='" + "test" + '\'' +
                ", begin=" + "1000-01-01 00:01:00.0" +
                ", end=" + "1000-01-01 00:01:00.0" +
                '}',bean.toString());
    }
}
