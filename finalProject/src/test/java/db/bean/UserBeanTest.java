package db.bean;

import com.epam.rd.khranovskyi.dao.bean.UserBean;
import org.junit.jupiter.api.Test;

import java.sql.Timestamp;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UserBeanTest {
    @Test
    void getActivityId() {
        UserBean bean = new UserBean();
        bean.setActivityId(25);
        assertEquals(25,bean.getActivityId());
    }

    @Test
    void setActivityId() {
        UserBean bean = new UserBean();
        bean.setActivityId(25);
        assertEquals(25,bean.getActivityId());
    }

    @Test
    void setActivityName() {
        UserBean bean = new UserBean();
        bean.setActivityName("Test");
        assertEquals("Test",bean.getActivityName());
    }

    @Test
    void getActivityName() {
        UserBean bean = new UserBean();
        bean.setActivityName("test");
        assertEquals("test",bean.getActivityName());
    }

    @Test
    void setUserLogin() {
        UserBean bean = new UserBean();
        bean.setUserLogin("aa");
        assertEquals("aa",bean.getUserLogin());
    }

    @Test
    void getUserLogin() {
        UserBean bean = new UserBean();
        bean.setUserLogin("aa");
        assertEquals("aa",bean.getUserLogin());
    }

    @Test
    void getUserActivityId() {
        UserBean bean = new UserBean();
        bean.setUserActivityId(25);
        assertEquals(25,bean.getUserActivityId());
    }

    @Test
    void setUserActivityId() {
        UserBean bean = new UserBean();
        bean.setUserActivityId(25);
        assertEquals(25,bean.getUserActivityId());
    }

    @Test
    void getUserId() {
        UserBean bean = new UserBean();
        bean.setUserId(25);
        assertEquals(25,bean.getUserId());
    }

    @Test
    void setUserId() {
        UserBean bean = new UserBean();
        bean.setUserId(25);
        assertEquals(25,bean.getUserId());
    }

    @Test
    void getStart() {
        UserBean bean = new UserBean();
        bean.setStart(Timestamp.valueOf("1000-01-01 00:01:00.0"));
        assertEquals(Timestamp.valueOf("1000-01-01 00:01:00.0"),bean.getStart());
    }

    @Test
    void setStart() {
        UserBean bean = new UserBean();
        bean.setStart(Timestamp.valueOf("1000-01-01 00:01:00.0"));
        assertEquals(Timestamp.valueOf("1000-01-01 00:01:00.0"),bean.getStart());
    }

    @Test
    void getStatus() {
        UserBean bean = new UserBean();
        bean.setStatus(2);
        assertEquals(2,bean.getStatus());
    }

    @Test
    void setStatus() {
        UserBean bean = new UserBean();
        bean.setStatus(2);
        assertEquals(2,bean.getStatus());
    }

    @Test
    void beanToString() {
        UserBean bean = new UserBean();
        bean.setActivityId(1);
        bean.setActivityName("test");
        bean.setUserId(1);
        bean.setUserLogin("test");
        bean.setStart(Timestamp.valueOf("1000-01-01 00:01:00.0"));
        bean.setEnd(Timestamp.valueOf("1000-01-01 00:01:00.0"));
        bean.setStatus(1);
        assertEquals("UserBean = [activityId = "+1+
                ", activity name = "+"test"+", userId = "+
                1+", login = "+"test"+", Start = "+
                "1000-01-01 00:01:00.0"+", end = "+"1000-01-01 00:01:00.0"+", status ="+1,bean.toString());
    }
}
