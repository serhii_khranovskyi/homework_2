<%@ page language="java" %>
<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>
<c:set var="title" value="List of activities" scope="page"/>
<%@ include file="/WEB-INF/jspf/head.jspf" %>
<%@include file="/WEB-INF/jspf/directive/register_style.jspf" %>


<body>
<nav class="navbar navbar-inverse" style=" background-color: #92D3E8;">
    <ul>
        <li style="margin: 30px"><a href="/accounting_time/user?command=myActivitiesCommand"> <fmt:message
                key="users_main_page_jsp.table.button.my_activities"/></a></li>
        <li class="helper"></li>
    </ul>
</nav>

<table border="1" style="width: 90%; margin: auto">

    <form action="user" method="post">
        <c:choose>
            <c:when test="${fn:length(userActivityBeans) == 0}">No such table</c:when>

            <c:otherwise>
                <tabel border="1">
                    <thead>
                    <tr>
                        <td><fmt:message key="activities_jsp.table.header.activity_name"/></td>
                        <td><fmt:message key="list_users_jsp.table.header.dateStart"/></td>
                        <td><fmt:message key="list_users_jsp.table.header.dateEnd"/></td>
                    </tr>
                    </thead>
                    <tbody class="data">

                    <c:forEach var="bean" items="${userActivityBeans}">
                        <tr>
                            <td>${bean.name}</td>
                            <td>${bean.begin}</td>
                            <td><p>${bean.end}</p></td>
                        </tr>


                    </c:forEach>
                    </tbody>
                </tabel>
            </c:otherwise>
        </c:choose>
    </form>

</table>


</body>
</html>
