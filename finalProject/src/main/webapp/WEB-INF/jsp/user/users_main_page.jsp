<%@ page language="java" %>
<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>
<c:set var="title" value="Categories list" scope="page"/>
<%@ include file="/WEB-INF/jspf/head.jspf" %>
<%@include file="/WEB-INF/jspf/directive/register_style.jspf" %>


<body>
<nav class="navbar navbar-inverse" style=" background-color: #92D3E8;">

    <ul>
        <li style="padding-right: 200px;  margin: 30px;"><a
                href="/accounting_time/user?command=profile"><fmt:message
                key="users_activities_jsp.head.link.profile"/></a></li>
        <li style=" margin: 30px;"><a href="/accounting_time/user?command=myActivitiesCommand"> <fmt:message
                key="users_main_page_jsp.table.button.my_activities"/></a></li>
        <li style=" margin: 30px; padding-left: 200px"><a href="/accounting_time/logout"><fmt:message
                key="list_users_jsp.table.header.logout"/></a></li>
        <li class="helper"></li>
    </ul>

</nav>

<table style="width: 90%; margin: auto">

    <h3 style="text-align: center"><fmt:message key="users_main_page_jsp.h1.header.categories"/></h3>
    <tr style="text-align: center; border: 1px">

        <c:choose>
            <c:when test="${fn:length(categoryBeanList) == 0}">No such table</c:when>

            <c:otherwise>

                <c:forEach var="bean" items="${categoryBeanList}">
                    <form id="choose_category" action="user" method="post" style="text-align: center;">
                        <div>
                            <p>${bean.name}</p>
                            <input type="hidden" name="command" value="selectedCategory"/>
                            <input type="hidden" name="categoryId" value="${bean.id}">
                            <input type="submit" value="select">
                        </div>
                    </form>
                </c:forEach>

            </c:otherwise>
        </c:choose>


    </tr>
</table>
<div style="margin-left: 550px;">
    <div style="display: flex">
        <c:if test="${currentPage != 1}">
            <form action="user" method="post">
                <input type="hidden" name="command" value="users_main_page">
                <input type="hidden" name="page" value="${currentPage - 1}">
                <input type="submit" value="Previous" class="btn btn-default color-blue" style="text-align: center">
            </form>

        </c:if>


        <c:forEach begin="1" end="${noOfPages}" var="i">
            <c:choose>
                <c:when test="${currentPage eq i}">
                    <input type="submit" value="${i}" class="btn btn-default colo"
                           style="border-color: darkblue;">
                </c:when>
                <c:otherwise>
                    <form action="user" method="post" style="text-align: center">
                        <input type="hidden" name="command" value="users_main_page">
                        <input type="hidden" name="page" value="${i}">
                        <input type="submit" value="${i}" class="btn btn-default color-blue" style="text-align: center">
                    </form>

                </c:otherwise>
            </c:choose>
        </c:forEach>
        <c:if test="${currentPage lt noOfPages}">

            <form action="user" method="post">
                <input type="hidden" name="command" value="users_main_page">
                <input type="hidden" name="page" value="${currentPage + 1}">
                <input type="submit" value="Next" class="btn btn-default color-blue" style="text-align: center">
            </form>

        </c:if>
    </div>
</div>
<%@ include file="/WEB-INF/jspf/footer.jspf" %>
</body>
</html>
