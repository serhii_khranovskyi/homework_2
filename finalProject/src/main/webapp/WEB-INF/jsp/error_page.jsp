<%@ page isErrorPage="true" %>
<%@ page import="java.io.PrintWriter" %>
<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>


<html>

<c:set var="title" value="Error" scope="page"/>

<body>

<table id="main-container">

    <tr>
        <td class="content">
            <%-- CONTENT --%>

            <h2 class="error">
                <fmt:message key="error_jsp.error_occurred"/>
            </h2>

            <%-- this way we get the error information (error 404)--%>
            <c:set var="code" value="<%=response.getStatus()%>"/>
            <c:set var="message" value="${exception['class'].name}"/>

            <%-- this way we get the exception --%>
            <c:set var="exception" value="${requestScope['javax.servlet.error.exception']}"/>

            <c:if test="${not empty code}">
                <h3><fmt:message key="error_jsp.code"/><%=response.getStatus()%></h3>
            </c:if>

            <c:if test="${not empty message}">
                <h3><fmt:message key="error_jsp.message"/> ${message} </h3>
            </c:if>
            <%-- if get this page using forward --%>
            <c:if test="${not empty errorMessage and empty exception and empty code}">
                <h3><fmt:message key="error_jsp.error_message"/> ${errorMessage}</h3>
            </c:if>

            <%-- this way we print exception stack trace --%>
            <c:if test="${not empty exception}">
                <hr/>
                <h3><fmt:message key="error_jsp.stack_trace"/></h3>
                <c:forEach var="stackTraceElement" items="${exception.stackTrace}">
                    ${stackTraceElement}
                </c:forEach>
            </c:if>
        </td>
    </tr>

</table>
</body>
</html>