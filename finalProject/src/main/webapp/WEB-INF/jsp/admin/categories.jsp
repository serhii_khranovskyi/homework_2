<%@ page language="java" %>
<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>
<c:set var="title" value="List activities" scope="page"/>
<%@ include file="/WEB-INF/jspf/head.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/register_style.jspf" %>


<body>
<nav class="navbar navbar-inverse" style=" background-color: #92D3E8;">
    <ul>
        <li style="margin: 30px;padding-left: 350px"><a href="/accounting_time/table?command=listUsers"><fmt:message
                key="users_activities_jsp.head.link.activities_user"/></a></li>
        <li style="margin: 30px;"><a href="/accounting_time/table?command=activitiesTable"><fmt:message
                key="list_users_jsp.table.header.activity"/></a></li>
        <li style="margin: 30px;"><a href="/accounting_time/table?command=usersTable"><fmt:message
                key="list_users_jsp.table.header.user"/></a></li>
        <li style="margin: 30px;padding-right: 350px"><a href="/accounting_time/logout"><fmt:message
                key="list_users_jsp.table.header.logout"/></a></li>

        <li class="helper"></li>
    </ul>
</nav>
<table id="main-container">
    <tr>
        <td class="modifyActivities">
            <form id="modify_activities" action="modify" method="post">

                <input type="hidden" name="command" value="modifyCategories"/>

                <legend>
                    <fmt:message key="activities_jsp.table.header.modify_categories"/>
                </legend>
                <select name="combobox">
                    <option value="0">Insert</option>
                    <option value="1">Update</option>
                    <option value="2">Delete</option>
                </select>
                <br/> <br/>
                <legend>

                    <fmt:message key="categories_jsp.table.header.category_id"/>
                    <input type="text" name="category_id"/>
                    <br/> <br/>

                    <fmt:message key="categories_jsp.table.header.category_name"/>
                    <input type="text" name="category_name"/>
                    <br/> <br/>

                </legend>

                <br/> <br/>

                <input type="submit" value='<fmt:message key="list_users_jsp.button.apply"/>'>
            </form>

        </td>

        <td class="modifyCategoryLocalization">
            <form action="modify" method="post">

                <input type="hidden" name="command" value="modifyCategoriesLocalization"/>

                <legend>
                    <fmt:message key="activities_jsp.table.header.modify_categories_translation"/>
                </legend>
                <select name="combobox_localization">
                    <option value="0">Insert</option>
                    <option value="1">Update</option>
                    <option value="2">Delete</option>
                </select>
                <br/> <br/>
                <legend>

                    <fmt:message key="categories_jsp.table.header.category_name"/>
                    <input type="text" name="category_name_localization"/>
                    <br/> <br/>

                    <fmt:message key="categories_jsp.table.header.categories_name_translation"/>
                    <input type="text" name="translation"/>
                    <br/> <br/>
                </legend>
                <br/> <br/>
                <input type="submit" value='<fmt:message key="list_users_jsp.button.apply"/>'>
            </form>


        </td>
        <td>
            <c:choose>
                <c:when test="${fn:length(languagesList) == 0}">No such table</c:when>

                <c:otherwise>
                    <div class="language">

                        <c:forEach var="bean" items="${languagesList}">
                            <input type="radio" name="language" value="${bean}">
                            <label for="${i}">${bean}</label>
                            <br/>
                        </c:forEach>

                    </div>
                </c:otherwise>
            </c:choose>
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>

            <c:choose>
                <c:when test="${fn:length(categoryList) == 0}">No such table</c:when>

                <c:otherwise>
                    <table id="list_user_table">
                        <thead>
                        <tr>
                            <td><fmt:message key="activities_jsp.table.header.activity_id"/></td>

                            <td><fmt:message key="activities_jsp.table.header.category"/></td>
                            <td><fmt:message key="activities_jsp.table.header.activity_name_translation"/></td>
                        </tr>
                        </thead>

                        <c:forEach var="bean" items="${categoryList}">

                            <tr>
                                <td>${bean.id}</td>
                                <td>${bean.name}</td>
                                <td>${bean.translation}</td>
                            </tr>

                        </c:forEach>
                    </table>
                </c:otherwise>
            </c:choose>

        </td>
    </tr>
</table>
<%@ include file="/WEB-INF/jspf/footer.jspf" %>
</body>
</html>