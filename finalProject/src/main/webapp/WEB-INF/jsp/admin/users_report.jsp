<%@ page language="java" %>
<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>
<c:set var="title" value="List users" scope="page"/>
<%@ include file="/WEB-INF/jspf/head.jspf" %>

<%@ include file="/WEB-INF/jspf/directive/register_style.jspf" %>


<body>
<nav class="navbar navbar-inverse" style=" background-color: #92D3E8;">
    <ul>
        <li style=" margin: 30px;"><a href="/accounting_time/table?command=usersTable"><fmt:message
                key="list_users_jsp.table.header.user"/></a></li>
        <li class="helper"></li>
    </ul>
</nav>

<table id="main-container">
    <tr>
        <td class="insert update delete activity_user">
            <c:choose>
                <c:when test="${fn:length(userList) == 0}">No such table</c:when>

                <c:otherwise>
                    <table id="list_order_table" class="data">
                        <thead class="data">
                        <tr>
                            <td><fmt:message key="users_jsp.table.header.mail"/></td>
                            <td><fmt:message key="users_jsp.table.header.firstName"/></td>
                            <td><fmt:message key="users_jsp.table.header.lastName"/></td>
                            <td><fmt:message key="users_jsp.table.header.password"/></td>
                            <td><fmt:message key="users_jsp.table.header.amountOfActivities"/></td>
                            <td><fmt:message key="users_jsp.table.header.amountOfTime"/></td>
                        </tr>
                        </thead>
                        <tbody class="data">
                        <c:forEach var="bean" items="${userList}">

                            <tr>
                                <td>${bean.mail}</td>
                                <td>${bean.first_name}</td>
                                <td>${bean.last_name}</td>
                                <td>${bean.password}</td>
                                <td>${bean.activitiesCount}</td>
                                <td>${bean.allActivitiesTime}</td>
                                <td>
                                    <form action="modify" method="post"/>
                                    <input type="hidden" name="command" value="detail">
                                    <input type="hidden" name="mail" value="${bean.mail}">
                                    <input type="submit" value="detail">
                                    </form>
                                </td>
                            </tr>

                        </c:forEach>
                        </tbody>
                    </table>
                </c:otherwise>
            </c:choose>

        </td>
    </tr>

</table>
<%@ include file="/WEB-INF/jspf/footer.jspf" %>
</body>
</html>
