<%@ page language="java" %>
<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>
<c:set var="title" value="List activities" scope="page"/>
<%@ include file="/WEB-INF/jspf/head.jspf" %>

<%@ include file="/WEB-INF/jspf/directive/register_style.jspf" %>

<body>
<nav class="navbar navbar-inverse" style=" background-color: #92D3E8;">
    <ul>
        <li style="margin: 30px;padding-left: 350px"><a href="/accounting_time/table?command=listUsers"><fmt:message
                key="users_activities_jsp.head.link.activities_user"/></a></li>
        <li style="margin: 30px;"><a href="/accounting_time/table?command=categoriesTable"> <fmt:message
                key="users_activities_jsp.head.link.categories"/></a></li>
        <li style="margin: 30px;"><a href="/accounting_time/table?command=usersTable"><fmt:message
                key="list_users_jsp.table.header.user"/></a></li>
        <li style="margin: 30px;padding-right: 350px"><a href="/accounting_time/logout"><fmt:message
                key="list_users_jsp.table.header.logout"/></a></li>
        <li class="helper"></li>
    </ul>
</nav>
<table id="main-container">

    <tr>
        <td class="modifyActivities" style="horiz-align: right">

            <form id="modify_activities" action="modify" method="post">

                <input type="hidden" name="command" value="modifyActivities"/>

                <legend>
                    <fmt:message key="activities_jsp.table.header.modify_activity"/>
                </legend>
                <select name="combobox">
                    <option value="0"><fmt:message key="list_users_jsp.body.combobox.insert"/></option>
                    <option value="1"><fmt:message key="list_users_jsp.body.combobox.update"/></option>
                    <option value="2"><fmt:message key="list_users_jsp.body.combobox.delete"/></option>
                </select>
                <br/> <br/>
                <legend>

                    <fmt:message key="activities_jsp.table.header.activity_id"/>
                    <input type="text" name="activity_id"/>
                    <br/> <br/>
                    <fmt:message key="activities_jsp.table.header.activity_name"/>
                    <input type="text" name="activity_name"/>
                    <br/> <br/>

                    <fmt:message key="activities_jsp.table.header.category"/>
                    <input type="text" name="category"/>
                    <br/> <br/>

                    <fmt:message key="activities_jsp.table.header.activity_name_translation"/>
                    <input type="text" name="activity_name"/>
                    <br/> <br/>

                    <fmt:message key="activities_jsp.table.header.category_translation"/>
                    <input type="text" name="category"/>
                    <br/> <br/>

                </legend>

                <br/> <br/>

                <input type="submit" value='<fmt:message key="list_users_jsp.button.apply"/>'>
            </form>

            <%-- CONTENT --%>

        </td>


        <td class="modifyActivitiesLocalization" style="horiz-align: right">
            <form id="modify_activities_localization" action="modify" method="post">

                <input type="hidden" name="command" value="modifyActivitiesLocalization"/>

                <legend>
                    <fmt:message key="activities_jsp.table.header.modify_activity_translation"/>
                </legend>
                <select name="combobox_localization">
                    <option value="0"><fmt:message key="list_users_jsp.body.combobox.insert"/></option>
                    <option value="1"><fmt:message key="list_users_jsp.body.combobox.update"/></option>
                    <option value="2"><fmt:message key="list_users_jsp.body.combobox.delete"/></option>
                </select>
                <br/> <br/>
                <legend>

                    <fmt:message key="activities_jsp.table.header.activity_name"/>
                    <input type="text" name="activity_name_localization"/>
                    <br/> <br/>

                    <fmt:message key="activities_jsp.table.header.activity_name_translation"/>
                    <input type="text" name="translation"/>
                    <br/> <br/>

                </legend>

                <br/> <br/>

                <input type="submit" value='<fmt:message key="list_users_jsp.button.apply"/>'>
            </form>

            <%-- CONTENT --%>

        </td>
        <td>
            <c:choose>
                <c:when test="${fn:length(languagesList) == 0}">No such table</c:when>

                <c:otherwise>
                    <div class="language">

                        <c:forEach var="bean" items="${languagesList}" varStatus="i">
                            <input type="radio" name="language" value="${bean}">
                            <label for="${i}">${bean}</label>
                            <br/>

                        </c:forEach>

                    </div>
                </c:otherwise>
            </c:choose>
        </td>
    </tr>
    <tr>
        <form action="modify" method="post">
            <td><input type="hidden" name="command" value="sortActivityByName"/>
                <input type="hidden" name="sortActivityByName" value="1">
                <input type="submit" value="<fmt:message key="activities_jsp.table.header.sort_activity_by_name"/>">
            </td>
        </form>
        <form action="modify" method="post">
            <td><input type="hidden" name="command" value="sortActivityByCategory"/>
                <input type="hidden" name="sortActivityByCategory" value="2">
                <input type="submit" value="<fmt:message key="activities_jsp.table.header.sort_activity_by_category"/>">
            </td>
        </form>
        <form action="modify" method="post">
            <td><input type="hidden" name="command" value="sortActivityByAmount"/>
                <input type="hidden" name="sortActivityByAmount" value="3">
                <input type="submit"
                       value="<fmt:message key="activities_jsp.table.header.sort_activity_by_amount_of_users"/>"></td>
        </form>

    </tr>
</table>
<div style="width: 100%; overflow: auto; height: 250px;">
    <table style="width: 90%; margin: auto">
        <tr>
            <td class="content">
                <%-- CONTENT --%>

                <c:choose>
                    <c:when test="${fn:length(activityList) == 0}">No such table</c:when>

                    <c:otherwise>
                        <table id="list_user_table">
                            <thead>
                            <tr>
                                <td><fmt:message key="activities_jsp.table.header.activity_id"/></td>
                                <td><fmt:message key="activities_jsp.table.header.activity_name"/></td>
                                <td><fmt:message key="activities_jsp.table.header.category"/></td>
                                <td><fmt:message key="activities_jsp.table.header.amount_of_users"/></td>
                            </tr>
                            </thead>

                            <c:forEach var="bean" items="${activityList}">

                                <tr>

                                    <td>${bean.id}</td>
                                    <td>${bean.name}</td>
                                    <td>${bean.category.getName()}</td>
                                    <td>${bean.nameTranslation}</td>
                                    <td>${bean.amountOfUsers}</td>
                                </tr>

                            </c:forEach>
                        </table>
                    </c:otherwise>
                </c:choose>

                <%-- CONTENT --%>
            </td>
        </tr>
    </table>
</div>
<%@ include file="/WEB-INF/jspf/footer.jspf" %>
</body>
</html>