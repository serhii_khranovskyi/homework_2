package com.epam.rd.khranovskyi.dao.entity;

public class Category extends Entity {
    private String name;
    private String nameTranslation;
    private int locale;

    public static Category createCategory(String name) {
        return new Category(name);
    }

    public Category(String name) {
        this.name = name;
    }

    public Category() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLocale() {
        return locale;
    }

    public void setLocale(int locale) {
        this.locale = locale;
    }

    public String getNameTranslation() {
        return nameTranslation;
    }

    public void setNameTranslation(String nameTranslation) {
        this.nameTranslation = nameTranslation;
    }

    @Override
    public String toString() {
        return "Category [" +
                "name=" + name + ']';
    }
}
