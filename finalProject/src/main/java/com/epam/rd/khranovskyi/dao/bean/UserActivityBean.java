package com.epam.rd.khranovskyi.dao.bean;

import com.epam.rd.khranovskyi.dao.entity.Entity;

import java.sql.Timestamp;

public class UserActivityBean extends Entity {
    private String name;
    private Timestamp begin;
    private Timestamp end;
    private Long allTime;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Timestamp getBegin() {
        return begin;
    }

    public void setBegin(Timestamp begin) {
        this.begin = begin;
    }

    public Timestamp getEnd() {
        return end;
    }

    public void setEnd(Timestamp end) {
        this.end = end;
    }

    public Long getAllTime() {
        return allTime;
    }

    public void setAllTime(Long allTime) {
        this.allTime = allTime;
    }

    @Override
    public String toString() {
        return "UserActivityBean{" +
                "name='" + name + '\'' +
                ", begin=" + begin +
                ", end=" + end +
                '}';
    }
}
