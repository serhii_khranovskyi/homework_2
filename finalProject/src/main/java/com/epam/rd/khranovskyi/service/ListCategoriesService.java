package com.epam.rd.khranovskyi.service;

import com.epam.rd.khranovskyi.dao.CategoryDAO;
import com.epam.rd.khranovskyi.dao.Path;
import com.epam.rd.khranovskyi.dao.bean.CategoryBean;
import org.apache.log4j.Logger;
import org.springframework.ui.Model;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.WebRequest;

import java.io.IOException;
import java.util.List;

public class ListCategoriesService {
    private static final Logger LOG = Logger.getLogger(ListCategoriesService.class);

    public String execute(Model model, WebRequest request) throws IOException {

        String language = (String) model.getAttribute("defaultLocale");
        model.addAttribute("defaultLocale", language);
        LOG.trace("Language -->" + language);

        String getPage = request.getParameter("page");
        LOG.trace("GetHtref--> " + getPage);
        List<CategoryBean> categoryBeanList = null;

        int page = 1;
        if (getPage != null)
            page = Integer.parseInt(getPage);
        int recordsPerPage = 2;

        CategoryDAO dao = new CategoryDAO();

        // Get number of records in db
        int noOfRecords = dao.getCategoryBeansCount();
        if (language.equals("en")) {
            LOG.trace("equals en");
            categoryBeanList = dao.getCategoryBeansTranslation((page - 1) * recordsPerPage, recordsPerPage, "English");
        } else if (language.equals("ru")) {
            categoryBeanList = dao.getCategoryBeans((page - 1) * recordsPerPage, recordsPerPage);
        }

        LOG.trace("Found in DB: CategoryList --> " + categoryBeanList);
        int noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
        request.setAttribute("categoryBeanList", categoryBeanList, RequestAttributes.SCOPE_REQUEST);
        //set attributes for pagination
        request.setAttribute("noOfPages", noOfPages, RequestAttributes.SCOPE_REQUEST);
        request.setAttribute("currentPage", page, RequestAttributes.SCOPE_REQUEST);

        return Path.PAGE__LIST_CATEGORIES;
    }
}
