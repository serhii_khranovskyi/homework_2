package com.epam.rd.khranovskyi.dao;

import com.epam.rd.khranovskyi.dao.bean.CategoryBean;
import com.epam.rd.khranovskyi.dao.entity.Category;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CategoryDAO {
    private static final Logger LOGGER = Logger.getLogger(CategoryDAO.class);

    private static final String SQL__TRANSACTION_CATEGORY_INSERT_CATEGORY =
            "insert into categories_localization (translation, language_id_language, categories_id_categories) values(?,?,?);";
    private static final String SQL__UPDATE_CATEGORY_BY_ID =
            "UPDATE categories SET category_name=? WHERE id_categories=?;";

    private static final String SQL__CATEGORY_INSERT_CATEGORY =
            "insert categories (category_name) value(?)";
    private static final String SQL__DELETE_CATEGORY_BY_ID =
            "DELETE FROM categories WHERE  id_categories = ?;";

    private static final String SQL__DELETE_CATEGORY
            = " delete from categories_localization where language_id_language = ? and categories_id_categories = ?; ";

    private static final String SQL__GET_CATEGORIES =
            "select id_categories, category_name, translation from categories left join categories_localization on categories_id_categories = id_categories ";

    private static final String SQL__GET_CATEGORIES_LIST =
            "select id_categories, category_name from categories limit ? , ?;";

    private static final String SQL__GET_CATEGORIES_TRANSLATION =
            "select categories_id_categories, translation from categories_localization, language  where language_id_language=id_language and name=? limit ? , ?;";

    private static final String SQL__GET_CATEGORY_AMOUNT = "SELECT COUNT(id_categories) as amount FROM categories;";

    private static final String SQL__GET_CATEGORY_ID_BY_NAME =
            "select id_categories from categories where category_name =?";

    private static final String SQL__GET_LANGUAGE_BY_NAME =
            "select  id_language from language where name = ?";

    private static final String SQL__CATEGORY_UPDATE_LOCALIZATION =
            "update categories_localization set translation  = ?, language_id_language = ? where categories_id_categories = ?;";

    /**
     * insert a category translation
     *
     * @param category
     * @param language
     */
    public void insertCategory(Category category, String language) {

        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();

            PreparedStatement getCategoryId = con.prepareStatement(SQL__GET_CATEGORY_ID_BY_NAME);
            getCategoryId.setString(1, category.getName());
            ResultSet resultSet = getCategoryId.executeQuery();
            resultSet.next();
            int categoryId = resultSet.getInt(1);
            resultSet.close();
            getCategoryId.close();


            PreparedStatement getLanguageId = con.prepareStatement(SQL__GET_LANGUAGE_BY_NAME);
            getLanguageId.setString(1, language);
            ResultSet resultSetLanguageId = getLanguageId.executeQuery();
            resultSetLanguageId.next();
            int languageId = resultSetLanguageId.getInt(1);
            resultSetLanguageId.close();
            getLanguageId.close();

            pstmt = con.prepareStatement(SQL__TRANSACTION_CATEGORY_INSERT_CATEGORY);
            pstmt.setString(1, category.getNameTranslation());
            pstmt.setInt(2, languageId);
            pstmt.setInt(3, categoryId);
            pstmt.executeUpdate();
            pstmt.close();

        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
            LOGGER.error("Error in insertCategory method -->" + ex);
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
    }

    /**
     * isert a category into Categories table
     *
     * @param category
     */
    public void insertCategory(Category category) {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();

            pstmt = con.prepareStatement(SQL__CATEGORY_INSERT_CATEGORY);
            pstmt.setString(1, category.getName());
            pstmt.executeUpdate();
            pstmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
    }

    /**
     * Overloaded method for updating the category
     *
     * @param category
     */

    public void updateCategory(Category category) {
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            updateCategory(con, category);
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
    }

    /**
     * Overloaded method for updating the category
     *
     * @param con
     * @param category
     * @throws SQLException
     */
    public void updateCategory(Connection con, Category category) throws SQLException {
        PreparedStatement pstmt = con.prepareStatement(SQL__UPDATE_CATEGORY_BY_ID);
        int k = 1;
        pstmt.setString(k++, category.getName());
        pstmt.setLong(k++, category.getId());
        pstmt.executeUpdate();
        pstmt.close();
    }

    /**
     * Overloaded method for updating the category
     *
     * @param category
     * @param language
     */
    public void updateCategory(Category category, String language) {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            //вынести в отдельный метод
            PreparedStatement getCategoryId = con.prepareStatement(SQL__GET_CATEGORY_ID_BY_NAME);
            getCategoryId.setString(1, category.getName());
            ResultSet resultSet = getCategoryId.executeQuery();
            resultSet.next();
            int categoryId = resultSet.getInt(1);
            resultSet.close();
            getCategoryId.close();
            //вынести в отдельный метод
            PreparedStatement getLanguageId = con.prepareStatement(SQL__GET_LANGUAGE_BY_NAME);
            getLanguageId.setString(1, language);
            ResultSet resultSetLanguageId = getLanguageId.executeQuery();
            resultSetLanguageId.next();
            int languageId = resultSetLanguageId.getInt(1);
            resultSetLanguageId.close();
            getLanguageId.close();

            PreparedStatement updateLocale = con.prepareStatement(SQL__CATEGORY_UPDATE_LOCALIZATION);
            updateLocale.setString(1, category.getNameTranslation());
            updateLocale.setInt(2, languageId);
            updateLocale.setInt(3, categoryId);

            updateLocale.executeUpdate();

        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
            LOGGER.trace("Error -->" + ex);
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
    }

    /**
     * delete the category by id
     *
     * @param category
     */
    public void deleteCategory(Category category) {
        PreparedStatement pstmt = null;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(SQL__DELETE_CATEGORY_BY_ID);
            pstmt.setLong(1, category.getId());
            pstmt.execute();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
    }

    /**
     * delete the category translation
     *
     * @param category
     * @param language
     */
    public void deleteCategory(Category category, String language) {
        PreparedStatement pstmt = null;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();


            PreparedStatement getCategoryId = con.prepareStatement(SQL__GET_CATEGORY_ID_BY_NAME);
            getCategoryId.setString(1, category.getName());
            ResultSet resultSet = getCategoryId.executeQuery();
            resultSet.next();
            int categoryId = resultSet.getInt(1);
            resultSet.close();
            getCategoryId.close();

            PreparedStatement getLanguageId = con.prepareStatement(SQL__GET_LANGUAGE_BY_NAME);
            getLanguageId.setString(1, language);
            ResultSet resultSetLanguageId = getLanguageId.executeQuery();
            resultSetLanguageId.next();
            int languageId = resultSetLanguageId.getInt(1);
            resultSetLanguageId.close();
            getLanguageId.close();

            pstmt = con.prepareStatement(SQL__DELETE_CATEGORY);
            pstmt.setLong(1, languageId);
            pstmt.setLong(2, categoryId);
            pstmt.execute();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
    }

    /**
     * get list of categories
     *
     * @return
     */
    public List<CategoryBean> getCategories() {
        ArrayList<CategoryBean> categoryArrayList = new ArrayList<>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            CategoryDAO.CategoryMapper1 mapper = new CategoryDAO.CategoryMapper1();
            pstmt = con.prepareStatement(SQL__GET_CATEGORIES);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                categoryArrayList.add(mapper.mapRow(rs));
            }
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
            LOGGER.error("Error in method getCategories. Message->" + ex);
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
        return categoryArrayList;
    }

    /**
     * Get list of categories beans
     *
     * @param start
     * @param total
     * @return
     */
    public List<CategoryBean> getCategoryBeans(int start, int total) {
        List<CategoryBean> categoryBeanList = new ArrayList<>();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            LOGGER.trace("Get connection");
            stmt = con.prepareStatement(SQL__GET_CATEGORIES_LIST);
            stmt.setInt(1, start);
            stmt.setInt(2, total);
            rs = stmt.executeQuery();
            CategoryDAO.CategoryMapper mapper = new CategoryDAO.CategoryMapper();
            while (rs.next())
                categoryBeanList.add(mapper.mapRow(rs));
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            LOGGER.error(ex);
            ex.printStackTrace();
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
        return categoryBeanList;
    }

    /**
     * get the count of categories records in db
     *
     * @return
     */
    public int getCategoryBeansCount() {
        PreparedStatement getCategoryAmount = null;
        ResultSet resultSet = null;
        Connection con = null;
        int categoryAmount = 0;
        try {
            con = DBManager.getInstance().getConnection();
            getCategoryAmount = con.prepareStatement(SQL__GET_CATEGORY_AMOUNT);
            resultSet = getCategoryAmount.executeQuery();
            resultSet.next();
            categoryAmount = resultSet.getInt("amount");
            getCategoryAmount.close();
            resultSet.close();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
        return categoryAmount;
    }

    /**
     * get the list of categories translation
     *
     * @param start
     * @param total
     * @param language
     * @return
     */
    public List<CategoryBean> getCategoryBeansTranslation(int start, int total, String language) {
        List<CategoryBean> categoryBeanList = new ArrayList<>();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Connection con = null;
        try {

            con = DBManager.getInstance().getConnection();
            stmt = con.prepareStatement(SQL__GET_CATEGORIES_TRANSLATION);
            LOGGER.trace("We get categories translation");
            stmt.setString(1, language);
            stmt.setInt(2, start);
            stmt.setInt(3, total);
            rs = stmt.executeQuery();
            CategoryDAO.CategoryMapperTranslation mapper = new CategoryDAO.CategoryMapperTranslation();
            while (rs.next()) {
                categoryBeanList.add(mapper.mapRow(rs));
            }
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            LOGGER.error("Error-->" + ex);
            ex.printStackTrace();
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
        return categoryBeanList;
    }

    /**
     * Extracts a category information from the result set row.
     */
    private static class CategoryMapper implements EntityMapper<CategoryBean> {

        @Override
        public CategoryBean mapRow(ResultSet rs) {
            try {
                CategoryBean bean = new CategoryBean();
                bean.setId(rs.getLong(Fields.CATEGORIES__ID_CATEGORIES));
                bean.setName(rs.getString(Fields.CATEGORIES__CATEGORY_NAME));
                return bean;
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    /**
     * Extracts a category information from the result set row.
     */
    private static class CategoryMapper1 implements EntityMapper<CategoryBean> {

        @Override
        public CategoryBean mapRow(ResultSet rs) {
            try {
                CategoryBean bean = new CategoryBean();
                bean.setId(rs.getLong(Fields.CATEGORIES__ID_CATEGORIES));
                bean.setName(rs.getString(Fields.CATEGORIES__CATEGORY_NAME));
                bean.setTranslation(rs.getString(3));
                return bean;
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    /**
     * Extracts a category translation from the result set row.
     */
    private static class CategoryMapperTranslation implements EntityMapper<CategoryBean> {

        @Override
        public CategoryBean mapRow(ResultSet rs) {
            try {
                CategoryBean bean = new CategoryBean();

                bean.setId(rs.getLong(Fields.ACTIVITY__CATEGORY_ID));
                bean.setName(rs.getString(Fields.CATEGORY_TRANSLATION));
                return bean;
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }
}
