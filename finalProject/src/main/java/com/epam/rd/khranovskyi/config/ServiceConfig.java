package com.epam.rd.khranovskyi.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({"com.epam.rd.khranovskyi.service"})
public class ServiceConfig {
}
