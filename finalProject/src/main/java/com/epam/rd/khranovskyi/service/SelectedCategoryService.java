package com.epam.rd.khranovskyi.service;

import com.epam.rd.khranovskyi.dao.ActivityDAO;
import com.epam.rd.khranovskyi.dao.Path;
import com.epam.rd.khranovskyi.dao.entity.Activity;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.WebRequest;

import java.io.IOException;
import java.util.List;

@Service
public class SelectedCategoryService {
    private static final Logger LOG = Logger.getLogger(SelectedCategoryService.class);

    public String execute(Model model, WebRequest request) throws IOException {
        Long userId = (Long) model.getAttribute("userId");
        LOG.trace("userId--> " + userId);

        String language = (String) model.getAttribute("defaultLocale");
        LOG.trace("Language -->" + language);

        String categoryId = request.getParameter("categoryId");
        if (categoryId == null || categoryId.isEmpty()) {
            categoryId = (String) model.getAttribute("categoryId");
        }
        LOG.trace("Parameter--> " + categoryId);
        List<Activity> activityList = null;
        //Get the activity list in English
        if (language.equals("en")) {
            language = "English";
            activityList = new ActivityDAO().getActivitiesTranslation(userId, categoryId, language);
            //Get the activity list in Russian
        } else if (language.equals("ru")) {
            activityList = new ActivityDAO().getActivities(userId, categoryId);
        }
        LOG.trace("Found in DB: userOrderBeanList --> " + activityList);
        request.setAttribute("activityList", activityList, RequestAttributes.SCOPE_REQUEST);
        LOG.trace("Set the request attribute: ActivityList --> " + activityList);
        model.addAttribute("categoryId", categoryId);
        LOG.debug("Commands finished");
        return Path.PAGE__LIST_ACTIVITIES;
    }
}
