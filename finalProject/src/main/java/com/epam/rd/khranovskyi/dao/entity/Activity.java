package com.epam.rd.khranovskyi.dao.entity;

public class Activity extends Entity {
    private String name;
    private String nameTranslation;
    private Category category;
    private Date date;
    private int locale;
    private int status;
    private int amountOfUsers;

    public static Activity createActivity(String name, Category category) {
        Activity activity = new Activity();
        activity.setName(name);
        activity.setCategory(category);
        return activity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getLocale() {
        return locale;
    }

    public int getStatus() {
        return status;
    }

    public int getAmountOfUsers() {
        return amountOfUsers;
    }

    public void setAmountOfUsers(int amountOfUsers) {
        this.amountOfUsers = amountOfUsers;
    }

    public String getNameTranslation() {
        return nameTranslation;
    }

    public void setNameTranslation(String nameTranslation) {
        this.nameTranslation = nameTranslation;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setLocale(int locale) {
        this.locale = locale;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        return sb.append("Activity[ name = ")
                .append(getName())
                .append("; categories = ")
                .append(getCategory()).append(getLocale()).toString();
    }
}
