package com.epam.rd.khranovskyi.service;

import com.epam.rd.khranovskyi.dao.Path;
import com.epam.rd.khranovskyi.dao.bean.SessionModel;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.context.request.WebRequest;

import java.io.IOException;

@Service
public class LogoutService {
    /**
     * Useless class
     *
     * @param model
     * @param webRequest
     * @return
     * @throws IOException
     */
    public String execute(Model model, WebRequest webRequest) throws IOException {
        SessionModel.deleteModel();
        return Path.PAGE__LOGIN;
    }

}
