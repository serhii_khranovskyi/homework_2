package com.epam.rd.khranovskyi.service;

import com.epam.rd.khranovskyi.dao.ActivityDAO;
import com.epam.rd.khranovskyi.dao.Path;
import com.epam.rd.khranovskyi.dao.bean.UserActivityBean;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.WebRequest;

import java.io.IOException;
import java.util.List;

@Service
public class UsersActivitiesService {
    private static final Logger LOG = Logger.getLogger(UsersActivitiesService.class);

    public String execute(Model model, WebRequest request) throws IOException {
        String language = (String) model.getAttribute("defaultLocale");
        LOG.trace("Language -->" + language);
        Long userId = (Long) model.getAttribute("userId");
        LOG.trace("Id -- >" + userId);
        String getPage = request.getParameter("page");
        LOG.trace("GetHtref--> " + getPage);
        List<UserActivityBean> userActivityBeans = null;
        int page = 1;
        if (getPage != null)
            page = Integer.parseInt(getPage);
        int recordsPerPage = 2;

        ActivityDAO activityDAO = new ActivityDAO();

        //Get number of records from db
        int noOfRecords = activityDAO.getAmountOfUserActivities(userId);
        LOG.trace("Number of Records-->" + noOfRecords);

        if (language.equals("en")) {
            LOG.trace("equals en");
            userActivityBeans = activityDAO.getActivityBeansTranslation((page - 1) * recordsPerPage, recordsPerPage, userId, "English");
        } else if (language.equals("ru")) {
            userActivityBeans = activityDAO.getActivityBeans((page - 1) * recordsPerPage, recordsPerPage, userId);
        }

        LOG.trace("Found in DB: userOrderBeanList --> " + userActivityBeans);
        int noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
        request.setAttribute("userActivityBeans", userActivityBeans, RequestAttributes.SCOPE_REQUEST);
        //Set attributes for pagination
        request.setAttribute("noOfPages", noOfPages, RequestAttributes.SCOPE_REQUEST);
        request.setAttribute("currentPage", page, RequestAttributes.SCOPE_REQUEST);
        LOG.trace("userActivityBeans --> " + userActivityBeans + "noOfPages --> " + noOfPages + "currentPage" + page);
        return Path.PAGE__LIST_USERS_ACTIVITIES;
    }
}
