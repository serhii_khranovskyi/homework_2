package com.epam.rd.khranovskyi.dao.entity;

import java.sql.Timestamp;

public class Request extends Entity {
    private String activityName;
    private String userName;
    private Timestamp start;
    private Timestamp end;

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Timestamp getStart() {
        return start;
    }

    public void setStart(Timestamp start) {
        this.start = start;
    }

    public Timestamp getEnd() {
        return end;
    }

    public void setEnd(Timestamp end) {
        this.end = end;
    }

    @Override
    public String toString() {
        return "Request{" +
                "activityName='" + activityName + '\'' +
                ", userName='" + userName + '\'' +
                ", start=" + start +
                ", end=" + end +
                '}';
    }
}
