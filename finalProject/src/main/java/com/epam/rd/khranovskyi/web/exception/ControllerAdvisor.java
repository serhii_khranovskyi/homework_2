package com.epam.rd.khranovskyi.web.exception;

import com.epam.rd.khranovskyi.dao.Path;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ControllerAdvisor {
    private static final Logger LOG = Logger.getLogger(ControllerAdvisor.class);

    @ExceptionHandler(value = NullPointerException.class)
    @ResponseStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE)
    public String handleNPE(Exception exception, Model model) {
        model.addAttribute("exception", exception);
        LOG.trace("exception-->" + exception + " exceptionMessage--> " + exception.getMessage() + " exceptionCause " + exception.getCause());
        return Path.PAGE__ERROR_PAGE;
    }

    @ExceptionHandler(value = IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String handleIllegalArgumentException(Exception exception, Model model) {
        model.addAttribute("exception", exception);
        return Path.PAGE__ERROR_PAGE;
    }

    @ExceptionHandler(value = HttpMessageNotReadableException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String handleHttpMessageNotReadableException(Exception exception, Model model) {
        model.addAttribute("exception", exception);
        return Path.PAGE__ERROR_PAGE;
    }

    @ExceptionHandler(value = ClientException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String handleClientException(Exception exception, Model model) {
        model.addAttribute("exception", exception);
        return Path.PAGE__ERROR_PAGE;
    }
}
