package com.epam.rd.khranovskyi.web.filter;

import com.epam.rd.khranovskyi.dao.Path;
import com.epam.rd.khranovskyi.dao.bean.SessionModel;
import com.epam.rd.khranovskyi.dao.entity.Role;
import org.apache.log4j.Logger;

import javax.servlet.*;
import java.io.IOException;
import java.util.*;


public class CommandAccessFilter implements Filter {
    private static final Logger LOG = Logger.getLogger(CommandAccessFilter.class);

    // commands access
    private static Map<Role, List<String>> accessMap = new HashMap<Role, List<String>>();
    private static List<String> commons = new ArrayList<String>();
    private static List<String> outOfControl = new ArrayList<String>();

    public void destroy() {
        LOG.debug("Filter destruction starts");
        // do nothing
        LOG.debug("Filter destruction finished");
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        LOG.debug("Filter starts");

        if (accessAllowed(request)) {
            LOG.debug("Filter finished");
            chain.doFilter(request, response);
        } else {
            String errorMessasge = "You do not have permission to access the requested resource";

            request.setAttribute("errorMessage", errorMessasge);
            LOG.trace("Set the request attribute: errorMessage --> " + errorMessasge);
            LOG.trace("Requset" + request.getLocalName());
            request.getRequestDispatcher(Path.PAGE__ERROR_PAGE)
                    .forward(request, response);
        }
    }

    private boolean accessAllowed(ServletRequest request) {

        String commandName = request.getParameter("command");

        if (outOfControl.contains(commandName)) {
            LOG.trace("You have permission because command is outOfControl");
            return true;
        }

        if (commandName == null || commandName.isEmpty()) {
            LOG.trace("You do not have permission because commandName ==null || isEmpty");
            return false;
        }

        if (SessionModel.getModel() == null) {
            LOG.trace("You have permission because session is null");
            return false;
        }
        Role userRole = null;
        if (SessionModel.getModel().getAttribute("userRole") == (Integer) 1) {
            userRole = Role.ADMIN;
        }
        if (SessionModel.getModel().getAttribute("userRole") == (Integer) 0) {
            userRole = Role.USER;
        }
        if (userRole == null) {
            LOG.trace("You have permission because Role is null");
            return false;
        }
        return accessMap.get(userRole).contains(commandName)
                || commons.contains(commandName);
    }

    public void init() throws ServletException {
        LOG.debug("Filter initialization starts");

        // roles
        String admin = "logout1 listUsers usersTable activitiesTable sortActivityByName sortActivityByCategory sortActivityByAmount categoriesTable modifyCategories modifyActivityUser modifyUsers modifyActivities modifyActivitiesLocalization modifyCategoriesLocalization usersReport detail Export";
        String user = "logout1 users_main_page myActivitiesCommand UpdateUserActivityCommand updateUserActivity selectedCategory selectedActivity deleteUserActivity users_main_page profile editProfile  language language=en language=ru historyUserActivity";
        String common = "logout noCommand viewSettings updateSettings language language=en language=ru";
        String out = "login register language language=en language=ru";

        accessMap.put(Role.ADMIN, asList(admin));
        accessMap.put(Role.USER, asList(user));

        LOG.trace("Access map --> " + accessMap);

        // commons
        commons = asList(common);
        LOG.trace("Common commands --> " + commons);

        // out of control
        outOfControl = asList(out);
        LOG.trace("Out of control commands --> " + outOfControl);

        LOG.debug("Filter initialization finished");
    }

    /**
     * Extracts parameter values from string.
     *
     * @param str parameter values string.
     * @return list of parameter values.
     */
    private List<String> asList(String str) {

        List<String> list = new ArrayList<String>();
        try {
            StringTokenizer st = new StringTokenizer(str);
            while (st.hasMoreTokens()) list.add(st.nextToken());
        } catch (Exception e) {
            LOG.error("Method asList-->" + e);
        }
        return list;

    }

}
