package com.epam.rd.khranovskyi.service;

import com.epam.rd.khranovskyi.dao.Path;
import com.epam.rd.khranovskyi.dao.UserDAO;
import com.epam.rd.khranovskyi.dao.entity.User;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.WebRequest;

import java.io.IOException;
import java.util.List;

@Service
public class UsersReportService {
    private static final Logger LOG = Logger.getLogger(UsersReportService.class);

    public String execute(Model model, WebRequest request) throws IOException {
        //get list of users information
        List<User> userList = new UserDAO().getUsersInfo();
        LOG.trace("Found in DB: userList");

        // put user list to request
        request.setAttribute("userList", userList, RequestAttributes.SCOPE_REQUEST);
        LOG.trace("Set the request attribute: userList is Empty-->" + userList.isEmpty());
        return Path.PAGE__USER_REPORT;
    }
}
