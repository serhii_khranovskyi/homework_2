package com.epam.rd.khranovskyi.web.controller;

import com.epam.rd.khranovskyi.service.*;
import com.epam.rd.khranovskyi.dao.bean.SessionModel;
import com.epam.rd.khranovskyi.web.exception.ClientException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;

import java.io.IOException;

@Controller
public class UserController {
    private Model model;

    @Autowired
    private EditProfileService editProfileService;

    public UserController() {
        model = SessionModel.getModel();
    }

    @RequestMapping("/user")
    public String user(@RequestParam String command, WebRequest webRequest) throws IOException, ClientException {
        if (command.equals("users_main_page")) return new ListCategoriesService().execute(model, webRequest);
        if (command.equals("myActivitiesCommand")) return new UsersActivitiesService().execute(model, webRequest);
        if (command.equals("updateUserActivity")) return new UpdateUserActivityService().execute(model, webRequest);
        if (command.equals("selectedCategory")) return new SelectedCategoryService().execute(model, webRequest);
        if (command.equals("deleteUserActivity")) return new DeleteUserActivityService().execute(model, webRequest);
        if (command.equals("profile")) return new ProfileService().execute(model, webRequest);
        if (command.equals("editProfile")) return editProfileService.execute(model, webRequest);
        if (command.equals("selectedActivity")) return new SelectedActivityService().execute(model, webRequest);
        if (command.equals("historyUserActivity"))
            return new HistoryUsersActivityService().execute(model, webRequest);
        return null;
    }
}
