package com.epam.rd.khranovskyi.service;

import com.epam.rd.khranovskyi.dao.ActivityDAO;
import com.epam.rd.khranovskyi.dao.Path;
import com.epam.rd.khranovskyi.dao.bean.UserBean;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.WebRequest;

import java.io.IOException;
import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Service
public class ListUsersService {
    private static final Logger LOG = Logger.getLogger(ListUsersService.class);

    private static final String SQL__GET_USER_BEANS = " select  id_activities_userscol, activities_id_activities, activity_name, users_id_users, mail, beginning, ending, status \n" +
            "            from activities_users, users, activities \n" +
            "            where activities_id_activities= id_activities AND users_id_users = id_users;";

    private static final String SQL__GET_USER_BEANS_TRANSLATION =
            " select  id_activities_userscol, activities_users.activities_id_activities, COALESCE(translation, 'unknown'), activities_users.users_id_users, mail, beginning, ending, status \n" +
                    "            from activities_users \n" +
                    "             join users on activities_users.users_id_users = id_users\n" +
                    "            join activities on activities_users.activities_id_activities= id_activities\n" +
                    "            \n" +
                    "           left  join activities_localization on activities_localization.activities_id_activities = id_activities;";

    private static class CompareById implements Comparator<UserBean>, Serializable {
        private static final long serialVersionUID = -1573481565177573283L;

        public int compare(UserBean bean1, UserBean bean2) {
            if (bean1.getUserId() > bean2.getUserId())
                return 1;
            else return -1;
        }
    }

    private static Comparator<UserBean> compareById = new CompareById();

    public String execute(Model model, WebRequest webRequest) throws IOException {

        LOG.debug("Commands starts");
        String language = (String) model.getAttribute("defaultLocale");
        LOG.trace("Language -->" + language);
        List<UserBean> userBeanList = null;
        if (language.equals("en")) {
            //Get list of users` activities with detail information in English
            userBeanList = new ActivityDAO().getUserBeans(SQL__GET_USER_BEANS_TRANSLATION, language);
        } else if (language.equals("ru")) {
            //Get list of users` activities with detail information
            userBeanList = new ActivityDAO().getUserBeans(SQL__GET_USER_BEANS);
        }

        LOG.trace("Found in DB: userOrderBeanList --> " + userBeanList);

        Collections.sort(userBeanList, compareById);

        // put the list to request
        webRequest.setAttribute("userBeanList", userBeanList, RequestAttributes.SCOPE_REQUEST);
        LOG.trace("Set the request attribute: userOrderBeanList --> " + userBeanList + System.lineSeparator() + " Size " + userBeanList.size());
        LOG.debug("Commands finished");
        return Path.PAGE__LIST_USERS;
    }

}
