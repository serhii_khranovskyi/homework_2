package com.epam.rd.khranovskyi.web.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Time {
    String DEFAULT_EXECUTION = "EXECUTE";

    String value() default DEFAULT_EXECUTION;
}
