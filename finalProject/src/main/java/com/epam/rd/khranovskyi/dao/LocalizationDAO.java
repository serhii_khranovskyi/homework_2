package com.epam.rd.khranovskyi.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class LocalizationDAO {

    private static final String SQL__GET_LANGUAGES_LIST =
            "Select name from language";

    /**
     * get the language list from the language table
     *
     * @return list of strings
     */
    public ArrayList<String> getLanguagesList() {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        ArrayList<String> arrayList = new ArrayList<>();
        try {
            con = DBManager.getInstance().getConnection();

            pstmt = con.prepareStatement(SQL__GET_LANGUAGES_LIST);
            rs = pstmt.executeQuery();
            while (rs.next()) {

                arrayList.add(rs.getString("name"));
            }
            pstmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
        return arrayList;
    }
}
